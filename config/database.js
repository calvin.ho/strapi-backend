module.exports = ({ env }) => {
  if (env("NODE_ENV") === "development") {
    return {
      defaultConnection: "default",
      connections: {
        default: {
          connector: 'bookshelf',
          settings: {
          client: 'sqlite',
          filename: env('DATABASE_FILENAME', '.tmp/data.db'),
          },
          options: {
          useNullAsDefault: true,
          },
        },
      },
    };
  } else {
    // In production, the collection types can't be created
    return {
      defaultConnection: "default",
      connections: {
        default: {
          connector: "bookshelf",
          settings: {
            client: "mysql",
            host: env("DATABASE_HOST", "10.146.0.2"),
            port: env.int("DATABASE_PORT", 3306),
            database: env("DATABASE_NAME", process.env.DATABASE_NAME),
            username: env("DATABASE_USERNAME", process.env.DATABASE_USERNAME),
            password: env("DATABASE_PASSWORD", process.env.DATABASE_PASSWORD),
          },
          options: {
            ssl: false,
          },
        },
      },
    };
  }
};
