module.exports = ({ env }) => ({
  email: {
    provider: "nodemailer",
    providerOptions: {
      host: env("SMTP_HOST", "smtp.mailgun.org"),
      port: env("SMTP_PORT", 587),
      auth: {
        user: env("SMTP_HOST_USER", process.env.SMTP_HOST_USER) || "postmaster@sandbox4add6630cc2b461c9200710609cec9a6.mailgun.org",
        pass: env("SMTP_HOST_PW", process.env.SMTP_HOST_PW) || "e90e1ed7a2a9a4d3186c2292753a91a5-baa55c84-9b2570e3",
      },
      // ... any custom nodemailer options
    },
    settings: {
      defaultFrom: "hello@example.com",
      defaultReplyTo: "hello@example.com",
    },
  }
});



