'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/services.html#core-services)
 * to customize this service
 */

module.exports = {
    async findOne(ctx) {
        console.log('find');
        const { id } = ctx.params;
        const entity = await strapi.services.shop.find({id});
        return sanitizeEntity(entity, { model: strapi.models.shop });
      },

};
